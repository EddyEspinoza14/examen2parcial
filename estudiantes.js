const bd =[
    {"Id":0,"cedula": "0987654321","Apellido":"Belen", "Nombre":"Soriano", "Semestre": "Quinto", "Paralelo":"A"},
    {"Id":1, "cedula": "0987654322","Apellido":"Monteagudo", "Nombre":"Jorge", "Semestre": "Quinto",   
    "Paralelo":"B", 
    "Correo":"jorge@gmail.com"},
    {"Id":2,"cedula": "0987654323","Apellido":"Sala", "Nombre":"Vanesa", "Semestre": "Quinto","Paralelo":"A"},
    {"Id":3, "cedula": "0987654324","Apellido":"Carrion", "Nombre":"Marina", "Semestre": "Quinto","Paralelo":"B"},
    {"Id":4, "cedula": "0997654321","Apellido":"Mendoza", "Nombre":"juana", "Semestre": "Quinto", "Paralelo":"C"},
    {"Id":5, "cedula": "0987654311","Apellido":"Damian", "Nombre":"Diaz", "Semestre": "Sexto",  "Paralelo":"A"},
    {"Id":6, "cedula": "0985654321","Apellido":"Alvez", "Nombre":"Jonathan", "Semestre": "Sexto","Paralelo":"B"},
    {"Id":7,"cedula": "0987644321","Apellido":"Marquez", "Nombre":"Gabriel", "Semestre": "Tercero", "Paralelo":"A"},
    {"Id":8, "cedula": "0977654321","Apellido":"Vinces", "Nombre":"Juan", "Semestre": "Cuarto",  "Paralelo":"A"},
    {"Id":9,"cedula": "0987655321","Apellido":"Bailon", "Nombre":"Aimar", "Semestre": "Cuarto", "Paralelo":"B"}
]

const estudiantes = document.querySelectorAll('.nombre-estudiante');

estudiantes.forEach((estudiante)=>{
    estudiante.addEventListener('click', (nombre)=>{
        let id=nombre.target.getAttribute('estu-id');
        bd.forEach((estudiante)=>{
            if(id == estudiante.Id){
                const verDetalle=nombre.target.parentElement.lastElementChild;
                verDetalle.innerHTML=`
                                    <div class="lista">
                                        <div class="nom">
                                            <h2>cedula:</h2>
                                            <p>${estudiante.cedula}</p>
                                        </div>

                                        <div class="nom">
                                        <h2>Nombre:</h2>
                                        <p>${estudiante.Nombre}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Apellido:</h2>
                                            <p>${estudiante.Apellido}</p>
                                        </div>
                                
                                        <div class="nom">
                                            <h2>Semestre:</h2>
                                            <p>${estudiante.Semestre}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Paralelo:</h2>
                                            <p>${estudiante.Paralelo}</p>
                                        </div> 
                                    </div>`

            }
        })
    })
})